/*
  A  
 B B 
C   C
 B B 
  A  
*/

const décalageA = 'A'.charCodeAt(0)

const alphabet = Array(26)
  .fill(null)
  .map((_, index) => String.fromCharCode(index + décalageA));

const quartDeDiamant = (taille: number): QuartDeDiamant => {
  return alphabet
    .map(ligneÀ(taille))
    .filter((ligne: string | undefined) => ligne)
    .map((ligne) => ligne!);
}

const index = (lettre: string): number => {
  return lettre.charCodeAt(0) - décalageA
}

const ligneÀ =
  (taille: number) =>
    (lettre: string): string | undefined => {
      if (taille <= index(lettre)) return undefined

      const s = ' '.repeat(taille - (index(lettre) + 1))
      const deuxièmeEspace = ' '.repeat(index(lettre))
      return s + lettre + deuxièmeEspace
    }

const renverseLaChaine = (str: string): string => {
  return str.split('').reverse().join('')
}

const miroirMagiqueDebout = (quartDeDiamant: QuartDeDiamant): DemiDiamant => {
  return quartDeDiamant.map(partieGaucheDeLaLigne => partieGaucheDeLaLigne + renverseLaChaine(partieGaucheDeLaLigne.slice(0, -1)))
}

const miroirMagiqueAllongé = (hautDuDiamant: Diamant): Diamant => {
  return hautDuDiamant.concat(hautDuDiamant.slice(0, -1).reverse());
}

type Diamant = string[];
type DemiDiamant = string[];
type QuartDeDiamant = string[];
const diamant = (taille: number): Diamant => {
  return miroirMagiqueAllongé(miroirMagiqueDebout(quartDeDiamant(taille)))
}

it('diamant vide', () => {
  expect(diamant(0)).toEqual([])
});

it('diamant taille 1', () => {
  expect(diamant(1)).toEqual(['A'])
});

it('diamant taille 2', () => {
  expect(diamant(2)).toEqual([
    ' A ',
    'B B',
    ' A ',
  ])
});

it('diamant taille 3', () => {
  expect(diamant(3)).toEqual([
    '  A  ',
    ' B B ',
    'C   C',
    ' B B ',
    '  A  ',
  ])
});

it('diamant taille 26', () => {
  expect(diamant(26)).toEqual([
    "                         A                         ",
    "                        B B                        ",
    "                       C   C                       ",
    "                      D     D                      ",
    "                     E       E                     ",
    "                    F         F                    ",
    "                   G           G                   ",
    "                  H             H                  ",
    "                 I               I                 ",
    "                J                 J                ",
    "               K                   K               ",
    "              L                     L              ",
    "             M                       M             ",
    "            N                         N            ",
    "           O                           O           ",
    "          P                             P          ",
    "         Q                               Q         ",
    "        R                                 R        ",
    "       S                                   S       ",
    "      T                                     T      ",
    "     U                                       U     ",
    "    V                                         V    ",
    "   W                                           W   ",
    "  X                                             X  ",
    " Y                                               Y ",
    "Z                                                 Z",
    " Y                                               Y ",
    "  X                                             X  ",
    "   W                                           W   ",
    "    V                                         V    ",
    "     U                                       U     ",
    "      T                                     T      ",
    "       S                                   S       ",
    "        R                                 R        ",
    "         Q                               Q         ",
    "          P                             P          ",
    "           O                           O           ",
    "            N                         N            ",
    "             M                       M             ",
    "              L                     L              ",
    "               K                   K               ",
    "                J                 J                ",
    "                 I               I                 ",
    "                  H             H                  ",
    "                   G           G                   ",
    "                    F         F                    ",
    "                     E       E                     ",
    "                      D     D                      ",
    "                       C   C                       ",
    "                        B B                        ",
    "                         A                         ",
  ])
});
